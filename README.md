# Better Recipes
Easier loading for recipes. Features inheritance, multiple workbench types, and recipe IDs.

## Documentation
Fires an event, `better-recipes:onRecipesReady` with the `recipeBuilder` object as the only argument.
Call `recipeBuilder.buildRecipe(recipeConfig)` with an object, `recipeConfig`, which will then be built and inherrited and all that good stuff.
The event is fired on all threads.
After that, everything is copied into the base game's recipe lists.

There might be actual documentation of the recipeConfigs later, for now just check `recipes/bases.js` for simple inheritance and the overall format.
There's also one example at the bottom of `recipes/recipeBuilder.js` that gives an example of how to make a potion item base and a specific potion (outdated example).
Also check `Isleward-Modding/ars-botanica` for examples of how to apply this to actual situations in modding.
