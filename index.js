let pathUtils = require('path');

module.exports = {
	name: 'Better Recipes',

	init: function () {
		let isMapThread = !!process.send;

		let baseThread = {
			require: this.require.bind(this),
			folderName: this.folderName,
			relativeFolderName: this.relativeFolderName
		};
		let commonThread = this.require('threads/common', true);

		let thread;

		if (isMapThread)
			thread = extend({}, baseThread, commonThread, this.require('threads/map', true));
		else
			thread = extend({}, baseThread, commonThread, this.require('threads/main', true));

		thread.events = this.events;

		thread.initCommon();
		thread.init();
	},

	require: function (path, local) {
		if (local)
			return require(pathUtils.join(process.cwd(), this.relativeFolderName, path));

		return require(pathUtils.join(process.cwd(), path));
	}
};
