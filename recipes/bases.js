module.exports = [
	// true base
	{
		id: 'base',
		type: ['*'],
		inherit: [],
		inputs: [],
		outputs: []
	},

	// attributes
	{
		id: 'attribute.worthless',
		type: [],
		inherit: ['base'],
		outputs: [{
			worth: 0
		}]
	},
	{
		id: 'attribute.notgear',
		type: [],
		inherit: ['base'],
		outputs: [{
			noSalvage: true,
			noAugment: true
		}]
	},
	{
		id: 'attribute.material',
		type: [],
		inherit: ['base'],
		outputs: [{
			inherit: ['attribute.worthless', 'attribute.notgear'],
			material: true
		}]
	},

	// others
	{
		id: 'attribute.consumable',
		type: ['*.consumable'],
		inherit: ['base'],
		outputs: [{
			inherit: ['attribute.worthless', 'attribute.notgear'],
			type: 'consumable',
			uses: 1,
			cdMax: 14
		}]
	}
];
