let recipeBuilder = {
	// Provided by `threads/common`. require(path, isLocal)
	require: null,

	recipes: {},
	categories: {},

	loadRecipes: function (toLoad = ['bases']) {
		toLoad.forEach(function (file) {
			let f = this.require('recipes/' + file, true);
			f.forEach(function (recipe) {
				this.buildRecipe(recipe);
			}, this);
		}, this);
	},

	buildRecipe: function (recipe) {
		let inherits = (recipe.inherit || []).map(i => this.recipes[i] || {});

		// Extend {} with inherits, and THEN recipe so that current recipe can overwrite inherited properties
		let newRecipe = extend({}, ...inherits, recipe);

		// EDIT: After writing all this out and thinking it through, I decided to implement item inherits.
		// They are used in Ars Botanica heavily and also in a few example 'bases' (attribute.material, I think).
		// Original thought process below.
		//
		// Extend on an items ARRAY may cause some weirdness... we may want to change the behavior in the future.
		// For now, it will apply the properties of the first item (in the base/attribute) to ONLY the first item in the new recipe.
		// This might help, but it might cause issues when it comes to multi-item recipes with inherits.
		// If inheriting on a recipe with multiple items, it might be better to define every property manually.
		// Otherwise, inherits should be per-item (so a recipe can output an item with the gear base and also the material base).
		// We might want to implement per-item inherits at some point. The other attributes of recipes aren't inherited anyways, besides ingredient requirements (which we might not want to inherit anyways, since inheriting an item with all the properties already set up could be enough).
		// I'm not completely sure about this.

		// Don't inherit the ID, inherits, hiding, name, or description
		newRecipe.id = recipe.id;
		newRecipe.inherit = recipe.inherit;
		newRecipe.name = recipe.name;
		if (recipe.hidden)
			newRecipe.hidden = recipe.hidden;
		if (recipe.description)
			newRecipe.description = recipe.description;

		// Custom inherit for types
		// (Make a list of all the types and delete duplicates.)
		let iTypes = [].concat(...(inherits.map(i => i.type || [])));
		newRecipe.type = [].concat(recipe.type, iTypes);
		newRecipe.type = newRecipe.type.slice()
			.sort(function (a, b) {
				return a > b;
			})
			.reduce(function (a, b) {
				if (a.slice(-1)[0] !== b) a.push(b);
				return a;
			}, []);

		// Custom inherit for inputs
		// "Item codes" for inherits are in the format `recipe:i:index` or `recipe:o:index`
		newRecipe.inputs = recipe.inputs || [];
		newRecipe.inputs = newRecipe.inputs.map(function (input) {
			let inputInherits = (input.inherit || []).map(function (itemCode) {
				let parts = itemCode.split(':');

				let ind = parts[2];

				// Default to output
				let mode = 'DEFAULT';

				if (['i', 'in', 'input', 'inputs'].includes(parts[1]))
					mode = 'inputs';

				if (['o', 'out', 'output', 'outputs'].includes(parts[1]))
					mode = 'outputs';

				if (mode === 'DEFAULT') {
					// if not i/o/in/out, use the second part for ind. allows for attribute.material:0 which will assume output
					ind = parts[1];
				}

				if (!ind) {
					// Falsey value -- undefined or 0. Default to 0 if no value given.
					// This makes it so that inheriting `attribute.material` on its own will inherit the first output of attrib.mat
					ind = 0;
				}

				return recipeBuilder.recipes[parts[0]][mode][ind];
			});
			return extend({}, ...inputInherits, input);
		});

		// Custom inherit for outputs
		newRecipe.outputs = recipe.outputs || [];
		newRecipe.outputs = newRecipe.outputs.map(function (output) {
			let outputInherits = (output.inherit || []).map(function (itemCode) {
				let parts = itemCode.split(':');

				let ind = parts[2];

				// Default to output
				let mode = 'DEFAULT';

				if (['i', 'in', 'input', 'inputs'].includes(parts[1]))
					mode = 'inputs';

				if (['o', 'out', 'output', 'outputs'].includes(parts[1]))
					mode = 'outputs';

				if (mode === 'DEFAULT') {
					// if not i/o/in/out, use the second part for ind. allows for attribute.material:0 which will assume output
					ind = parts[1];
					mode = 'outputs';
				}

				if (!ind) {
					// Falsey value -- undefined or 0. Default to 0 if no value given.
					// This makes it so that inheriting `attribute.material` on its own will inherit the first output of attrib.mat
					ind = 0;
				}

				return recipeBuilder.recipes[parts[0]][mode][ind];
			});
			return extend({}, ...outputInherits, output);
		});

		// Register the recipe in the categories
		newRecipe.type.forEach(function (type) {
			// Ensure the category exists
			this.categories[type] = this.categories[type] || {};
			// Register the recipe
			this.categories[type][newRecipe.id] = newRecipe;
		}, this);

		// Register in the full list of recipes (used for inheritance lookups)
		this.recipes[recipe.id] = newRecipe;

		return newRecipe;
	}
};

module.exports = recipeBuilder;

// Some example recipes. Outdated format (missing name/description property on recipe and items as an array)
/*
console.log(module.exports.buildRecipe({
	id: 'basePotion',
	type: ['alchemy'],
	inherit: [],
	item: {
		type: 'consumable',
		worth: 0,
		noSalvage: true,
		noAugment: true,
		uses: 1,
		cdMax: 85
	},
	materials: []
}));

console.log(module.exports.buildRecipe({
	id: 'minorHealthPotion',
	type: [],
	inherit: ['basePotion'],
	item: {
		name: 'Minor Healing Potion',
		description: 'Does not affect emotional scars.',
		effects: [
			{
				type: 'gainStat',
				rolls: {
					stat: 'hp',
					amount: '50%'
				}
			}
		]
	},
	materials: [
		{
			name: 'Skyblossom',
			quantity: 3
		},
		{
			name: 'Empty Vial',
			quantity: 1
		}
	]
}));

console.log(module.exports)
*/

/* EXAMPLE RECIPE

CUSTOM FIELDS:
	id: String, a unique identifier for the recipe
	type: Array, crafting stations to be included into
	inherit: Array, will extend other recipes onto this one

{
	'item': {
		'name': 'Fabled Carp on a Stick',
		'type': 'consumable',
		'sprite': [
			0,
			9
		],
		'description': 'It's a fish on a stick, what more do you want to know?',
		'worth': 0,
		'noSalvage': true,
		'noAugment': true,
		'uses': 1,
		'effects': [
			{
				'type': 'gainStat',
				'rolls': {
					'stat': 'hp',
					'amount': 200
				}
			}
		]
	},
	'materials': [
		{
			'quantity': 1,
			'name': 'Fabled Sun Carp'
		},
		{
			'name': 'Skewering Stick',
			'quantity': 1
		}
	]
}

{
	'item': {
		'name': 'Minor Healing Potion',
		'type': 'consumable',
		'sprite': [
			0,
			1
		],
		'description': 'Does not affect emotional scars.',
		'worth': 0,
		'noSalvage': true,
		'noAugment': true,
		'uses': 1,
		'cdMax': 85,
		'effects': [
			{
				'type': 'gainStat',
				'rolls': {
					'stat': 'hp',
					'amount': '50%'
				}
			}
		]
	},
	'materials': [
		{
			'name': 'Skyblossom',
			'quantity': 3
		},
		{
			'name': 'Empty Vial',
			'quantity': 1
		}
	]
}
*/
