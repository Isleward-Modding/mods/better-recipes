module.exports = {
	initCommon: function () {
		this.events.on('onBeforeGetRecipes', this.onBeforeGetRecipes.bind(this));
	},

	onBeforeGetRecipes: function (recipes) {
		// Load recipes
		this.recipes = this.require('recipes/recipeBuilder', true);
		this.recipes.require = this.require;
		this.recipes.loadRecipes();
		this.events.emit('better-recipes:onRecipesReady', this.recipes);

		// Load into base game
		Object.keys(this.recipes.categories).forEach(function (type) {
			let cat = this.recipes.categories[type];
			let r = Object.values(cat).map(function (filtering) {
				let i = extend({}, filtering);

				i.items = i.outputs;
				i.materials = i.inputs;

				i.outputs = null;
				i.inputs = null;

				delete i.outputs;
				delete i.inputs;

				if (i.items && i.items.length > 0 && i.items[0].name && !i.hidden) {
					delete i.hidden;
					delete i.inherit;
					return i;
				}
			}).filter(function (current) {
				// If falsey, there's no recipe returned from map, which means it isn't a valid recipe.
				return current;
			});
			recipes[type] = [].concat(recipes[type] || [], r);
		}, this);
	}
};
